package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import MainPackage.Account;
import MainPackage.Person;

import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class WithdrawWindow extends JFrame implements Serializable{

	private JPanel contentPane;
	private JTextField amountField;
	private JList accountsList;
	private JList personsList;
	private MainWindow mainWindow = new MainWindow();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					WithdrawWindow frame = new WithdrawWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public WithdrawWindow() {
		setTitle("Withdraw from account");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		personsList = setPersonsList();
		personsList.setBounds(10, 11, 203, 172);
		contentPane.add(personsList);
		
		accountsList = new JList();
		accountsList.setBounds(223, 11, 201, 172);
		contentPane.add(accountsList);
		
		amountField = new JTextField();
		amountField.setBounds(178, 194, 172, 20);
		contentPane.add(amountField);
		amountField.setColumns(10);
		
		JLabel lblAmount = new JLabel("Amount");
		lblAmount.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAmount.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblAmount.setBounds(77, 194, 91, 14);
		contentPane.add(lblAmount);
		
		JButton btnNewButton = new JButton("Withdraw");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Person person = (Person) personsList.getSelectedValue();
				Account account = (Account) accountsList.getSelectedValue();
				mainWindow.bank.removeAccount(person, account);
				String amountText =  amountField.getText();
				if(mainWindow.bank.isNumber(amountText)==false)
				{
					JOptionPane.showMessageDialog(null, "Error! Wrong type inserted into field. Try again!");
				}
				else
				{
					int amount = Integer.parseInt(amountText);
					if(account.withdrawMoney(amount) == false)
					{
						JOptionPane.showMessageDialog(null, "The amount to be withdrawn is greater than the balance! Try again!");
					}
					mainWindow.bank.addAccount(person, account);
					mainWindow.bank.writeAccounts();
					mainWindow.setAccountsTable();
					mainWindow.setVisible(true);
					dispose();
				}
			}
		});
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 14));
		btnNewButton.setBounds(10, 228, 414, 20);
		contentPane.add(btnNewButton);
	}

	public JList setPersonsList()
	{
		ArrayList<Person> persons = mainWindow.bank.getPersons();
		DefaultListModel<Person> personsList = new DefaultListModel<Person>();
		for(int i = 0;i<persons.size();i++)
			personsList.addElement(persons.get(i));
		final JList personList = new JList(personsList);
		personList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				Person person = (Person) personList.getSelectedValue();
				ArrayList<Account> accounts = mainWindow.bank.getBank().get(person);
				DefaultListModel<Account> accountLists = new DefaultListModel<Account>();
				for(int i = 0;i<accounts.size();i++)
					accountLists.addElement(accounts.get(i));
				accountsList.setModel(accountLists);
			}
		});
		return personList;
	}
}
