package GUI;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import MainPackage.Account;
import MainPackage.Bank;
import MainPackage.Person;
import MainPackage.SavingAccount;

import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MainWindow extends JFrame implements Serializable{

	private JPanel contentPane;
	private JTable accountsTable;
	private JTable personsTable;
	public Bank bank = new Bank();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow frame = new MainWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainWindow() {
		try {
			bank.readAccounts();
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		setTitle("Bank of TUC-N");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 900, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(20, 11, 318, 439);
		contentPane.add(scrollPane);
		
		accountsTable = new JTable();
		scrollPane.setViewportView(accountsTable);
		accountsTable.setModel(new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
					"Number","Holder","Type","Balance"
				}
				));
		setAccountsTable();
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(539, 11, 335, 439);
		contentPane.add(scrollPane_1);
		personsTable = new JTable();
		scrollPane_1.setViewportView(personsTable);
		personsTable.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"ID","First name","Last name","Address"
			}
			));
		setPersonsTable();
		
		JButton btnAddAccount = new JButton("Add account");
		btnAddAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddAccountWindow add = new AddAccountWindow();
				add.setVisible(true);
				dispose();
			}
		});
		btnAddAccount.setFont(new Font("Times New Roman", Font.BOLD, 14));
		btnAddAccount.setBounds(348, 11, 181, 41);
		contentPane.add(btnAddAccount);
		
		JButton btnRemoveAccount = new JButton("Remove Account");
		btnRemoveAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RemoveAccountWindow remove = new RemoveAccountWindow();
				remove.setVisible(true);
				dispose();
			}
		});
		btnRemoveAccount.setFont(new Font("Times New Roman", Font.BOLD, 14));
		btnRemoveAccount.setBounds(348, 63, 181, 41);
		contentPane.add(btnRemoveAccount);
		
		JButton btnDeposit = new JButton("Deposit");
		btnDeposit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DepositWindow window = new DepositWindow();
				window.setVisible(true);
				dispose();
			}
		});
		btnDeposit.setFont(new Font("Times New Roman", Font.BOLD, 14));
		btnDeposit.setBounds(348, 115, 181, 41);
		contentPane.add(btnDeposit);
		
		JButton btnWithdraw = new JButton("Withdraw");
		btnWithdraw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				WithdrawWindow window = new WithdrawWindow();
				window.setVisible(true);
				dispose();
			}
		});
		btnWithdraw.setFont(new Font("Times New Roman", Font.BOLD, 14));
		btnWithdraw.setBounds(348, 167, 181, 41);
		contentPane.add(btnWithdraw);
		
		JButton btnGenerateReport = new JButton("Generate Report");
		btnGenerateReport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Desktop.getDesktop().open(bank.file.outputFile);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnGenerateReport.setFont(new Font("Times New Roman", Font.BOLD, 14));
		btnGenerateReport.setBounds(348, 219, 181, 41);
		contentPane.add(btnGenerateReport);
	}
	
	public void setPersonsTable()
	{
		ArrayList<Person> persons = bank.getPersons();
		DefaultTableModel model = (DefaultTableModel) personsTable.getModel();
		Object[] rows = new Object[4];
		int i;
		model.setRowCount(0);
		for(i=0;i<persons.size();i++)
		{
			rows[0] = persons.get(i).getpID();
			rows[1] = persons.get(i).getFirstName();
			rows[2] = persons.get(i).getLastName();
			rows[3] = persons.get(i).getAddress();
			model.addRow(rows);
		}
	}
	
	public void setAccountsTable()
	{
		ArrayList<Person> persons = bank.getPersons();
		DefaultTableModel model = (DefaultTableModel) accountsTable.getModel();
		Object[] rows = new Object[4];
		model.setRowCount(0);
		for(Person pers: persons)
		{
			ArrayList<Account> accounts = bank.getBank().get(pers);
			for(Account acc: accounts)
			{
				rows[0] = acc.getAccountNo();
				rows[1] = pers.toString();
				if(acc instanceof SavingAccount)
					rows[2] = "Saving";
				else
					rows[2] = "Spending";
				rows[3] = acc.getBalance();
				model.addRow(rows);
			}
		}
	}
}
