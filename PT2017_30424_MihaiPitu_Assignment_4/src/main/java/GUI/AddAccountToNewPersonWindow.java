package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import MainPackage.Account;
import MainPackage.Person;
import MainPackage.SavingAccount;
import MainPackage.SpendingAccount;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AddAccountToNewPersonWindow extends JFrame {

	private JPanel contentPane;
	private JTextField idTextField;
	private JTextField firstNameField;
	private JTextField lastNameField;
	private JTextField addressField;
	private MainWindow mainWindow = new MainWindow();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddAccountToNewPersonWindow frame = new AddAccountToNewPersonWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddAccountToNewPersonWindow() {
		setTitle("Insert New Account");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 271, 294);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		idTextField = new JTextField();
		idTextField.setBounds(88, 11, 143, 20);
		contentPane.add(idTextField);
		idTextField.setColumns(10);
		
		JLabel lblId = new JLabel("ID");
		lblId.setHorizontalAlignment(SwingConstants.RIGHT);
		lblId.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblId.setBounds(32, 14, 46, 14);
		contentPane.add(lblId);
		
		firstNameField = new JTextField();
		firstNameField.setBounds(88, 42, 143, 20);
		contentPane.add(firstNameField);
		firstNameField.setColumns(10);
		
		lastNameField = new JTextField();
		lastNameField.setBounds(88, 73, 143, 20);
		contentPane.add(lastNameField);
		lastNameField.setColumns(10);
		
		addressField = new JTextField();
		addressField.setBounds(88, 105, 143, 20);
		contentPane.add(addressField);
		addressField.setColumns(10);
		
		JLabel lblFirstName = new JLabel("First Name");
		lblFirstName.setHorizontalAlignment(SwingConstants.RIGHT);
		lblFirstName.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblFirstName.setBounds(10, 45, 68, 14);
		contentPane.add(lblFirstName);
		
		JLabel lblLastName = new JLabel("Last Name");
		lblLastName.setHorizontalAlignment(SwingConstants.RIGHT);
		lblLastName.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblLastName.setBounds(10, 76, 68, 14);
		contentPane.add(lblLastName);
		
		JLabel lblAddress = new JLabel("Address");
		lblAddress.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblAddress.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAddress.setBounds(10, 108, 68, 14);
		contentPane.add(lblAddress);
		
		JRadioButton rdbtnSpendingAccount = new JRadioButton("Spending Account");
		rdbtnSpendingAccount.setBounds(88, 132, 143, 23);
		contentPane.add(rdbtnSpendingAccount);
		
		final JRadioButton rdbtnSavingAccount = new JRadioButton("Saving Account");
		rdbtnSavingAccount.setBounds(88, 155, 143, 23);
		contentPane.add(rdbtnSavingAccount);
		
		ButtonGroup accounts = new ButtonGroup();
		accounts.add(rdbtnSpendingAccount);
		accounts.add(rdbtnSavingAccount);
		
		JButton btnAddAccount = new JButton("Add new Account");
		btnAddAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String idString = idTextField.getText();
				String firstName = firstNameField.getText();
				String lastName = lastNameField.getText();
				String address = addressField.getText();
				if(mainWindow.bank.isNumber(idString)==false)
				{
					JOptionPane.showMessageDialog(null, "Error! Wrong type of ID! Try again!");
				}
				else
				{	
					int id = Integer.parseInt(idString);
					Account account = new Account();
					int x = mainWindow.bank.getNoOfAccounts();
					if(rdbtnSavingAccount.isSelected())
						account = new SavingAccount(x+1);
					else
						account = new SpendingAccount(x+1);
					Person person = new Person(id,firstName,lastName,address);
					mainWindow.bank.addAccount(person, account);
					mainWindow.bank.writeAccounts();
					mainWindow.setPersonsTable();
					mainWindow.setAccountsTable();
					mainWindow.setVisible(true);
					dispose();
				}
			}
		});
		btnAddAccount.setFont(new Font("Times New Roman", Font.BOLD, 14));
		btnAddAccount.setBounds(10, 185, 235, 59);
		contentPane.add(btnAddAccount);
	}
}
