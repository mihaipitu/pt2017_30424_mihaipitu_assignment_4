package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import MainPackage.Account;
import MainPackage.Bank;
import MainPackage.Person;
import MainPackage.SavingAccount;
import MainPackage.SpendingAccount;

import javax.swing.JList;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
import java.awt.event.ActionEvent;

public class AddAccountWindow extends JFrame implements Serializable{

	private JPanel contentPane;
	private JList personList;
	private MainWindow mainWindow = new MainWindow();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddAccountWindow frame = new AddAccountWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddAccountWindow() {
		
		setTitle("Add account into bank");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		final JList personList = setPersonsList();
		personList.setBounds(10, 11, 272, 219);
		contentPane.add(personList);
		
		final JRadioButton rdbtnSpendingAccount = new JRadioButton("Spending Account");
		rdbtnSpendingAccount.setBounds(288, 8, 140, 23);
		contentPane.add(rdbtnSpendingAccount);
		
		final JRadioButton rdbtnSavingAccount = new JRadioButton("Saving Account");
		rdbtnSavingAccount.setBounds(288, 34, 140, 23);
		contentPane.add(rdbtnSavingAccount);
		
		ButtonGroup accounts = new ButtonGroup();
		accounts.add(rdbtnSpendingAccount);
		accounts.add(rdbtnSavingAccount);
		
		JButton btnAddAccount = new JButton("Add Account");
		btnAddAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Person person = (Person) personList.getSelectedValue();
				int x = mainWindow.bank.getNoOfAccounts();
				Account account = new Account();
				if(rdbtnSpendingAccount.isSelected())
				{
					account = new SpendingAccount(x+1);
				}
				else
				{
					account = new SavingAccount(x+1);
				}
				mainWindow.bank.addAccount(person, account);
				mainWindow.bank.writeAccounts();
				mainWindow.setAccountsTable();
				mainWindow.setVisible(true);
				dispose();
			}
		});
		btnAddAccount.setFont(new Font("Times New Roman", Font.BOLD, 14));
		btnAddAccount.setBounds(292, 64, 136, 47);
		contentPane.add(btnAddAccount);
		
		JButton btnAddNewPerson = new JButton("Add new person");
		btnAddNewPerson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddAccountToNewPersonWindow window = new AddAccountToNewPersonWindow();
				window.setVisible(true);
				dispose();
			}
		});
		btnAddNewPerson.setFont(new Font("Times New Roman", Font.BOLD, 14));
		btnAddNewPerson.setBounds(292, 122, 136, 47);
		contentPane.add(btnAddNewPerson);
	}
	
	public JList setPersonsList()
	{
		ArrayList<Person> persons = mainWindow.bank.getPersons();
		DefaultListModel<Person> personsList = new DefaultListModel<Person>();
		for(int i = 0;i<persons.size();i++)
			personsList.addElement(persons.get(i));
		JList personList = new JList(personsList);
		return personList;
	}
}
