package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import MainPackage.Account;
import MainPackage.Person;

import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class DepositWindow extends JFrame implements Serializable {

	private JPanel contentPane;
	private JTextField amountField;
	private JList personsList;
	private JList accountsList;
	private MainWindow mainWindow = new MainWindow();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DepositWindow frame = new DepositWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DepositWindow() {
		setTitle("Deposit into account");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 387, 309);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		personsList = setPersonsList();
		personsList.setBounds(10, 11, 175, 182);
		contentPane.add(personsList);
		
		accountsList = new JList();
		accountsList.setBounds(195, 11, 169, 182);
		contentPane.add(accountsList);
		
		amountField = new JTextField();
		amountField.setBounds(137, 204, 175, 20);
		contentPane.add(amountField);
		amountField.setColumns(10);
		
		JLabel lblAmount = new JLabel("Amount");
		lblAmount.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblAmount.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAmount.setBounds(58, 204, 69, 20);
		contentPane.add(lblAmount);
		
		JButton btnNewButton = new JButton("Deposit");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Person person = (Person) personsList.getSelectedValue();
				Account account = (Account) accountsList.getSelectedValue();
				mainWindow.bank.removeAccount(person, account);
				String amountText =  amountField.getText();
				if(mainWindow.bank.isNumber(amountText)==false)
				{
					JOptionPane.showMessageDialog(null, "Error! Wrong type inserted into field. Try again!");
				}
				else
				{
					int amount = Integer.parseInt(amountText);
					account.depositMoney(amount);
					mainWindow.bank.addAccount(person, account);
					mainWindow.bank.writeAccounts();
					mainWindow.setAccountsTable();
					mainWindow.setVisible(true);
					dispose();
				}
			}
		});
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 14));
		btnNewButton.setBounds(10, 235, 351, 23);
		contentPane.add(btnNewButton);
	}
	
	public JList setPersonsList()
	{
		ArrayList<Person> persons = mainWindow.bank.getPersons();
		DefaultListModel<Person> personsList = new DefaultListModel<Person>();
		for(int i = 0;i<persons.size();i++)
			personsList.addElement(persons.get(i));
		final JList personList = new JList(personsList);
		personList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				Person person = (Person) personList.getSelectedValue();
				ArrayList<Account> accounts = mainWindow.bank.getBank().get(person);
				DefaultListModel<Account> accountLists = new DefaultListModel<Account>();
				for(int i = 0;i<accounts.size();i++)
					accountLists.addElement(accounts.get(i));
				accountsList.setModel(accountLists);
			}
		});
		return personList;
	}
}
