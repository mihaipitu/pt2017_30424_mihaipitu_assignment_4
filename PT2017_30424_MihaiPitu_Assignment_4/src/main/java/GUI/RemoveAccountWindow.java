package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import MainPackage.Account;
import MainPackage.Person;

import javax.swing.JList;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

public class RemoveAccountWindow extends JFrame implements Serializable{

	private JPanel contentPane;
	private final JList personsList;
	private final JList accountsList;
	private MainWindow mainWindow = new MainWindow();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RemoveAccountWindow frame = new RemoveAccountWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RemoveAccountWindow() {
		setTitle("Remove account from bank");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 389, 366);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		personsList = setPersonsList();
		personsList.setBounds(10, 11, 171, 239);
		contentPane.add(personsList);
		
		accountsList = new JList();
		accountsList.setBounds(191, 11, 171, 239);
		contentPane.add(accountsList);
		
		JButton btnRemoveAccount = new JButton("Remove Account");
		btnRemoveAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Person person = (Person) personsList.getSelectedValue();
				Account account = (Account) accountsList.getSelectedValue();
				mainWindow.bank.removeAccount(person, account);
				mainWindow.bank.writeAccounts();
				mainWindow.setAccountsTable();
				mainWindow.setVisible(true);
				dispose();
			}
		});
		btnRemoveAccount.setFont(new Font("Times New Roman", Font.BOLD, 14));
		btnRemoveAccount.setBounds(10, 266, 352, 50);
		contentPane.add(btnRemoveAccount);
	}

	public JList setPersonsList()
	{
		ArrayList<Person> persons = mainWindow.bank.getPersons();
		DefaultListModel<Person> personsList = new DefaultListModel<Person>();
		for(int i = 0;i<persons.size();i++)
			personsList.addElement(persons.get(i));
		final JList personList = new JList(personsList);
		personList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				Person person = (Person) personList.getSelectedValue();
				ArrayList<Account> accounts = mainWindow.bank.getBank().get(person);
				DefaultListModel<Account> accountLists = new DefaultListModel<Account>();
				for(int i = 0;i<accounts.size();i++)
					accountLists.addElement(accounts.get(i));
				accountsList.setModel(accountLists);
			}
		});
		return personList;
	}
}
