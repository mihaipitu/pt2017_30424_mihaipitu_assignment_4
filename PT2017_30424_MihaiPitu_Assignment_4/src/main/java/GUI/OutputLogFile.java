package GUI;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

public class OutputLogFile {
	File outputFile = new File("outputLog.txt");
	public OutputLogFile()
	{
		setOutputToFile();
	}
	
	public void setOutputToFile()
	{
	try{
		if(!outputFile.exists())
		{
			outputFile.createNewFile();
		}
		PrintStream output = new PrintStream(outputFile);
		System.setOut(output);
	}catch(IOException e){
		System.out.println(OutputLogFile.class.getName());
	}
	
	
}
	
	
}
