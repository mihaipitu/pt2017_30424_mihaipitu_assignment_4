package MainPackage;

import java.io.Serializable;

public class SpendingAccount extends Account implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private static int freeTransactions = 5;
	private static double transactionFee = 0.5;
	private int transactionCount;
	
	public SpendingAccount(int accountNo){
		super(accountNo);
		setTransactionCount(0);
	}
	
	public int getTransactionCount() {
		return transactionCount;
	}
	public void setTransactionCount(int transactionCount) {
		this.transactionCount = transactionCount;
	}
	
	public void deposit(double amount){
		transactionCount++;
		if(transactionCount > freeTransactions)
			amount -= transactionFee;
		this.depositMoney(amount);
	}
	
	public boolean withdraw(double amount){
		transactionCount++;
		if(transactionCount > freeTransactions)
			amount+=transactionFee;
	    return this.withdrawMoney(amount);
	}
	
	public boolean equals(SpendingAccount account)
	{
		return super.equals(account);
	}
}
