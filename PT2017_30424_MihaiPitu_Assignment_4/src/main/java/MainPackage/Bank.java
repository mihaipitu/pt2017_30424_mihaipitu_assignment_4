package MainPackage;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import GUI.OutputLogFile;


public class Bank implements BankProc, Serializable{

	private static final long serialVersionUID = 1L;
	private HashMap<Person, ArrayList<Account>> bank;
	public OutputLogFile file = new OutputLogFile();
	
	public Bank()
	{
		this.bank = new HashMap<Person,ArrayList<Account>>();
	}

	public void addAccount(Person person, Account account) {
		assert person!= null;
		assert account!= null;
		if(bank.get(person) == null)
		{
			ArrayList<Account> accounts = new ArrayList<Account>();
			accounts.add(account);
			bank.put(person,accounts);
		}
		else
		{
			ArrayList<Account> accounts = bank.remove(person);
			accounts.add(account);
			bank.put(person, accounts);
		}
		assert isWellFormed();
	}

	public void removeAccount(Person person, Account account) {
		assert person!=null;
		assert account!=null;
		ArrayList<Account> accounts = bank.remove(person);
		accounts.remove(account);
		bank.put(person, accounts);
		assert isWellFormed();
	}

	public void writeAccounts() {
		try
		{
			FileOutputStream outputFile = new FileOutputStream("bank.ser");
			ObjectOutputStream outputStream = new ObjectOutputStream(outputFile);
			outputStream.writeObject(bank);
			outputStream.close();
			outputFile.close();
		} catch(IOException e){
			e.printStackTrace();
		}
	}

	public void readAccounts() throws ClassNotFoundException {
		try
		{
			FileInputStream inputFile = new FileInputStream("bank.ser");
			ObjectInputStream inputStream = new ObjectInputStream(inputFile);
			bank = new HashMap<Person,ArrayList<Account>>();
			Map<Person,ArrayList<Account>> banks = (HashMap<Person,ArrayList<Account>>) inputStream.readObject();
			ArrayList<Person> personsList = new ArrayList<Person>();
			personsList.addAll(banks.keySet());
			ArrayList<Account> accounts;
			for(Person pers: personsList)
			{
				accounts = new ArrayList<Account>();
				accounts.addAll(banks.get(pers));
				for(Account acc: accounts)
				{
					addAccount(pers,acc);
				}
			}
			inputStream.close();
		} catch(IOException e){
			e.printStackTrace();
		}
		
	}

	public int getNoOfAccounts() {
		ArrayList<Person> persons = getPersons();
		int x=0;
		for(Person p:persons)
		{
			x+= bank.get(p).size();
		}
		return x;
	}
	
	public ArrayList<Person> getPersons()
	{
		ArrayList<Person> persons = new ArrayList<Person>();
		persons.addAll(bank.keySet());
		return persons;
	}
	
	public HashMap<Person,ArrayList<Account>> getBank()
	{
		return this.bank;
	}
	
	public boolean isWellFormed()
	{
		Set personsSet = bank.keySet();
		if(personsSet.size()!= getPersons().size())
			return false;
		int x =0;
		ArrayList<Person> persons = new ArrayList<Person>();
		persons.addAll(personsSet);
		for(Person p: persons)
		{
			x+= bank.get(p).size();
		}
		if(x!=getNoOfAccounts())
			return false;
		return true;
	}
	
	public boolean isNumber(String s)
	{
		for(int i=0;i<s.length();i++)
		{
			if(Character.isDigit(s.charAt(i))==false)
				return false;
		}
		return true;
	}
}
