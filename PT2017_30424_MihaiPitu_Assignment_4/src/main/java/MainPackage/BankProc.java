package MainPackage;

import java.util.ArrayList;

public interface BankProc {
	public void addAccount(Person person,Account account);
	public void removeAccount(Person person,Account account);
	public void readAccounts() throws ClassNotFoundException;
	public void writeAccounts();
	public int getNoOfAccounts();
	public boolean isWellFormed();
}
