package MainPackage;

import java.io.Serializable;

public class SavingAccount extends Account implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private static double interestRate = 0.035;
	public static double minBalance = 0.0;
	public static double minAmount = 1.0;
	private static int noWithdrawals = 5;
	private int withdrawalCount; 
	
	public SavingAccount(int accountNo) {
		super(accountNo);
		setWithdrawalCount(0);
	}

	public int getWithdrawalCount() {
		return withdrawalCount;
	}

	public void setWithdrawalCount(int withdrawalCount) {
		this.withdrawalCount = withdrawalCount;
	}
	
	public void deposit(double amount)
	{
		this.depositMoney(amount*(1+interestRate));
	}
	
	public boolean withdraw(double amount)
	{
		this.withdrawalCount++;
		if(withdrawalCount<noWithdrawals)
			return this.withdrawMoney(amount);
		else
		{
			System.out.println("Error! The number of allowed withdrawals is 0!");
			return false;
		}
	}
	
	public boolean equals(SavingAccount account)
	{
		return super.equals(account);
	}

}
