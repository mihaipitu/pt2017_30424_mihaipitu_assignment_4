package MainPackage;

import java.io.Serializable;
import java.util.Observable;

public class Account extends Observable implements Serializable{

	private static final long serialVersionUID = 1L;
	private int accountNo;
	private double balance;
	
	public Account()
	{
		this.accountNo = 777;
	}
	
	public Account(int accountNo){
		this.setAccountNo(accountNo);
		this.balance = 0;
	}

	public int getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(int accountNo) {
		this.accountNo = accountNo;
	}

	public double getBalance()
	{
		return this.balance;
	}
	
	public void depositMoney(double amount)
	{
		this.balance += amount;
		setChanged();
		notifyObservers();
	}
	public boolean withdrawMoney(double amount) 
	{
		if(this.balance>=amount)
		{
			this.balance-=amount;
			setChanged();
			notifyObservers();
			return true;
		}
		else
		{
			notifyObservers();
			System.out.println("Error! Amount bigger than the balance!");
			return false;
		}
	}
	
	public String toString()
	{
		String s;
		if(this == null)
			return "Invalid account";
		s = this.accountNo + " Balance: " + this.balance;
		return s;
	}
	
	public boolean equals(Account account)
	{
		if(account == null)
			return false;
		if(accountNo != account.getAccountNo())
			return false;
		return true;
	}
}
