package MainPackage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class Person implements Observer,Serializable{
	private static final long serialVersionUID = 1L;
	private int pID;
	private String firstName;
	private String lastName;
	private String address;
	
	
	public Person()
	{
		this.setpID(0);
		this.firstName = "asafa";
		this.lastName = "asafa";
		this.address = "narnia";
	}
	
	public Person(int pID,String firstName,String lastName,String address){
		this.setpID(pID);
		this.setFirstName(firstName);
		this.setLastName(lastName);
		this.setAddress(address);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@Override
	public String toString()
	{
		String s;
		s = this.getFirstName() + " " + this.getLastName();
		return s;
	}

	public int getpID() {
		return pID;
	}

	public void setpID(int pID) {
		this.pID = pID;
	}
	
	@Override
	public int hashCode()
	{
		Integer hash= new Integer(pID);
		return hash.hashCode();
	}
    
	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(o == null || getClass()!= o.getClass())
			return false;
		Person pers  = (Person) o;
		if(pers.getpID()!= pID)
			return false;
		if(pers.getFirstName()!= firstName)
			return false;
		if(pers.getLastName()!= lastName)
			return false;
		if(pers.getAddress()!= address)
			return false;
		return true;
	}

	public void update(Observable arg0, Object arg1) {
		String message = this.toString() + arg1.toString();
		System.out.println(message);
	}
}
